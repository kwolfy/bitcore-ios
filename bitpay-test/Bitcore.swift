//
//  Bitcore.swift
//  bitpay-test
//
//  Created by руслан on 18.09.17.
//  Copyright © 2017 bolicorp. All rights reserved.
//

import Foundation
import JavaScriptCore


class Bitcore {
    var api: JSValue!;
    var context: JSContext!;
    
    
    // инициализация с помощью json из Bicore.export()
    init(withJsonCredits: String, isProduction: Bool) {
        context = Bitcore.getBicoreWalletClientContext();
        if(isProduction) {
            context.evaluateScript("global.TestNet = false");
        }
        
        api = (context?.objectForKeyedSubscript("window").objectForKeyedSubscript("createApiFromData")
            .call(withArguments: [withJsonCredits]))!;
    }
    
    init(api: JSValue) {
        context = Bitcore.getBicoreWalletClientContext();
        self.api = api;
    }
    
    static func fromMnemonic(words: String, complete: @escaping (BitcoreError?, Bitcore?) -> ()){
        let context: JSContext = Bitcore.getBicoreWalletClientContext();
        
        let callback: @convention(block) (String, JSValue) -> () = { err, api in
            if(err != "null" && err != "undefined") {
                var errObj: BitcoreError = BitcoreError(withCode: BitcoreErrorCode.Unknown);
                //"bwc.ErrorINVALID_BACKUP: Invalid Backup."
                
                if(err == "bwc.ErrorINVALID_BACKUP: Invalid Backup.") {
                    errObj = BitcoreError(withCode: BitcoreErrorCode.InvalidBackup);
                }
                
                complete(errObj, nil);
                return ;
            }
            
            let bitcore = Bitcore(api: api);
            
            complete(nil, bitcore);
        }
        
        let args: [Any] = [words, unsafeBitCast(callback, to: AnyObject.self)];
        
        context.objectForKeyedSubscript("window").objectForKeyedSubscript("createApiFromMnemonic")
            .call(withArguments: args);
    }
    
    static func createNew(complete: @escaping (BitcoreError?, Bitcore?) -> ()){
        let context: JSContext = Bitcore.getBicoreWalletClientContext();
        
        let callback: @convention(block) (String, JSValue) -> () = { err, api in
            if(err != "null" && err != "undefined") {
                let errObj: BitcoreError = BitcoreError(withCode: BitcoreErrorCode.Unknown);
                
                complete(errObj, nil);
                return ;
            }
            
            let bitcore = Bitcore(api: api);
            complete(nil, bitcore);
        }
        
        context.objectForKeyedSubscript("window").objectForKeyedSubscript("createApiWithNewWallet")
            .call(withArguments: [unsafeBitCast(callback, to: AnyObject.self)]);
    }
    
    static var _context: JSContext? = nil;
    static func getBicoreWalletClientContext() -> JSContext {
        if(_context == nil) {
            _context = self.createBitcoreWalletClientContext();
        }
        
        return _context!;
    }
    
    private static func createBitcoreWalletClientContext() -> JSContext {
        // load the javascript file as a String
        let path = Bundle.main.path(forResource: "bundle", ofType: "js");
        
        var jsSource = try! String(contentsOfFile: path!);
        // Make browserify work
        jsSource = "var window = this; \(jsSource)"
        
        
        
        // create a javascript context environment and evaluate the script
        let _context: JSContext = JSContext()
        
        let xhr = XMLHttpRequest();
        let timers = WTWindowTimers();
        
        xhr.extend(_context);
        timers.extend(_context);
        
        _context.exceptionHandler = { (ctx: JSContext!, value: JSValue!) in
            // type of String
            let stacktrace = value.objectForKeyedSubscript("stack").toString()
            // type of Number
            let lineNumber = value.objectForKeyedSubscript("line")
            // type of Number
            let column = value.objectForKeyedSubscript("column")
            let moreInfo = "in method \(stacktrace)Line number in file: \(lineNumber), column: \(column)"
            print("JS ERROR: \(value) \(moreInfo)")
        }
        
        
        _context.evaluateScript(jsSource);
        
        return _context;
    }
    
    func export() -> String {
        let obj: JSValue = self.api.invokeMethod("export", withArguments: []);
        return obj.toString();
    }
    
    func getAddresses(complete: @escaping (BitcoreError?, [BitcoreAddress]? ) -> ()) {
        self.invokeMethod(method: "getMainAddresses", args: []) {err, result in
            if(err == nil) {
                let addressesArr: [[String: Any]] = result?.toArray() as! [[String: Any]];
                var addresses: [BitcoreAddress] = [];
                
                for item in addressesArr {
                    addresses.append(BitcoreAddress.init(withDictionary: item));
                }
                
                complete(nil, addresses);
            }
            else {
                complete(err, nil);
            }
        };
    }
    
    func createAddress(complete: @escaping (BitcoreError?, BitcoreAddress? ) -> ()) {
        
        self.invokeMethod(method: "createAddress", args: []) { err, result in
            if(err != nil) {
                complete(err, nil);
            }
            else {
                complete(nil, BitcoreAddress(withDictionary: result?.toDictionary() as! [String : Any]));
            }
        }
    }
    
    func sendMoney(
        toAddress: String, amount: Int32, message: String,
        complete: @escaping (BitcoreError?, BitcoreTransaction? ) -> ()) {
        
        let opts: [String : Any] = [
            "toAddress": toAddress,
            "amount": amount,
            "message": message
        ];
        
    
        self.invokeMethod(method: "sendMoney", args: [opts]) { err, result in
            if(err != nil) {
                complete(err, nil);
            }
            else {
                let txp = BitcoreTransaction(dictionary: result!.toDictionary() as! [String: Any]);
                complete(nil, txp);
            }
        }
    }
    
    
    func setNotificationListener(
        interval: UInt32,
        onNotification: @escaping (BitcoreNotification) -> ()) {
        
        // create the callback and cast to AnyObject
        let callback: @convention(block) (JSValue) -> () = { result in
            print(result);
            onNotification(BitcoreNotification(withDictionary: result.toDictionary() as! [String : Any]));
        }
        
        let args: [Any] = [unsafeBitCast(callback, to: AnyObject.self), interval];
        self.api.invokeMethod("onNotification", withArguments: args);
        
    }
    
    
    // подписка на уведомления о подтверждении транзакции
    func subscribeTxConfirmation(txId: String, complete: @escaping (BitcoreError?, JSValue?) -> ()) {
        self.invokeMethod(method: "subscribeTxConfirmation", args: [txId], resolve: complete);
    }
    
    func unsubscribeTxConfirmation(txId: String, complete: @escaping (BitcoreError?, JSValue?) -> ()) {
        self.invokeMethod(method: "unsubscribeTxConfirmation", args: [txId], resolve: complete);
    }
    
    
    private func invokeMethod(method: String, args: [Any], resolve: @escaping (BitcoreError?, JSValue?) -> ()) {
        // create the callback and cast to AnyObject
        let callback: @convention(block) (String, JSValue) -> () = {err, result in
            if(err != "null" && err != "undefined") {
                var errObj: BitcoreError = BitcoreError(withCode: BitcoreErrorCode.Unknown);
                
                if(err == "bwc.ErrorINSUFFICIENT_FUNDS: Insufficient funds") {
                    errObj = BitcoreError(withCode: BitcoreErrorCode.InsufficientFunds)
                }
                else if(err == "bwc.ErrorINVALID_BACKUP: Invalid Backup.") {
                    errObj = BitcoreError(withCode: BitcoreErrorCode.InvalidBackup);
                }
                
                resolve(errObj, nil);
                
                return ;
            }
            
            print(result)
            
            resolve(nil, result);
        }
        
        let args2 = args + [unsafeBitCast(callback, to: AnyObject.self)];
        
        self.api.invokeMethod(method, withArguments: args2);
    }
    
}

enum BitcoreNetworkType {
    case TestNet
    case LiveNet
};

enum BitcoreCoinType {
    case Btc
};

enum BitcoreErrorCode {
    case InsufficientFunds // недостаточно средств
    case Unknown // неизвестная ошибка
    case InvalidBackup // невалидная мнемоника
}

class BitcoreError {
    var code: BitcoreErrorCode;
    
    init(withCode: BitcoreErrorCode) {
        code = withCode;
    }
}

struct BitcoreTransaction {
    var id: String; // уникальный ид транзакции на сервере
    var coin: BitcoreCoinType; // всегда btc (в будущем может быть ещё ethereum)
    var txid: String; // уникальный ид транзакции в биткоин сети
    var createdOn: UInt32; // время создания транзакции
    var network: BitcoreNetworkType; // тип сети, тестовая сеть или реальная
    var amount: UInt32; // сумма перевода в Сатоши
    var message: String; // комментарий
    var status: String; // статус транзакции
    var fee: UInt32; // размер комиссии
    
    // есть ещё куча полей, но пока что это всё что необходимо
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as! String;
        self.coin = BitcoreCoinType.Btc;
        self.txid = dictionary["txid"] as! String;
        self.createdOn = dictionary["createdOn"] as! UInt32;
        self.network = dictionary["network"] as! String == "testnet" ? BitcoreNetworkType.TestNet : BitcoreNetworkType.LiveNet;
        self.amount = dictionary["amount"] as! UInt32;
        self.message = dictionary["message"] as! String;
        self.status = dictionary["status"] as! String;
        self.fee = dictionary["fee"] as! UInt32;
    }
}




struct BitcoreNotification {
    var createdOn: UInt32; // время генерации события
    var id: String; // уникальный ид события
    var data: [String: Any]; // информация о событии
    var type: BitcoreNotificationType;
    
    init(withDictionary: [String: Any]) {
        self.createdOn = withDictionary["createdOn"] as! UInt32;
        self.id = withDictionary["id"] as! String;
        self.data = withDictionary["data"] as! [String: Any];
        
        let type = withDictionary["type"] as! String;
        if(type == "NewIncomingTx") {
            self.type = BitcoreNotificationType.NewIncomingTx;
        }
        else if(type == "NewBlock") {
            self.type = BitcoreNotificationType.NewBlock;
        }
        else {
            self.type = BitcoreNotificationType.NewBlock;
        }
    }
}

struct BitcoreNotificationNewIncomingTxData {
    var txid: String;
    var address: String;
    var amount: UInt32;
    
    init(withDictionary: [String: Any]) {
        self.txid = withDictionary["txid"] as! String;
        self.address = withDictionary["address"] as! String;
        self.amount = withDictionary["amount"] as! UInt32;
    }
}

enum BitcoreNotificationType {
    case NewIncomingTx // получена новая транзакция
    case NewBlock // сгенерирован новый блок
}


struct BitcoreAddress {
    var address: String;
    var publicKeys: [String];
    var createdOn: UInt32;
    
    init(withDictionary: [String: Any]) {
        self.address = withDictionary["address"] as! String;
        self.publicKeys = withDictionary["publicKeys"] as! [String];
        self.createdOn = withDictionary["createdOn"] as! UInt32;
    }
}

/*
 
 
 { version: '1.0.0',
 createdOn: 1506601815,
 address: 'mvEBVqQ9RUA3fTEkJgZ2VCWGCZYKxeSYby',
 walletId: 'dd932fdc-3c3d-4fd0-b22a-b22dbc23de99',
 isChange: false,
 path: 'm/0/12',
 publicKeys: [ '03b56fbf05a81ba8468ba90265abe507eaa1ecccf82d82f707d6319e5c49b14ebb' ],
 coin: 'btc',
 network: 'testnet',
 type: 'P2PKH',
 _id: '59cceb578a6d704b1a534707' }
 
 */


/*
 {
 version: '1.0.0',
 createdOn: 1506103459,
 id: '015061034599970000',
 type: 'NewIncomingTx',
 data:
 { txid: '3ff5c36d22d3fbd362a60c4a18c4c6303048e91f15ee46e8c30e27af8aca3694',
 address: 'mhN5NDKJxWMeton2swpCCB3MavEsc8GvTa',
 amount: 30000 },
 walletId: '380b2efa-e05e-4e50-a19e-46c6033de45f',
 creatorId: null
 }
 */

