//
//  ViewController.swift
//  bitpay-test
//
//  Created by руслан on 18.09.17.
//  Copyright © 2017 bolicorp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btn: UIButton!
    
    @IBOutlet weak var addressTxt: UITextField!
    var bitcore: Bitcore? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        importFromData();
        
        bitcore?.getAddresses() {err, addresses in
            print(addresses);
        };
        
        
        /*
         Пример создания адреса
         bitcore?.createAddress(){err, address in
            print(address);
         };
        */
        
        //self.sendMoneyExample();
        //self.listenNotificationsExample();
        
        //self.listenNotificationsExample();
        
        //importFromMnemonic();
        //createNew();
        
        //ВНИМАНИЕ: createNew и importFromMnemonic выполняются ассинхронно, 
        //Поэтому функции self.sendMoneyExample и self.listenNotificationsExample можно выполнять непосредственно после завершения, см. код ниже
        
    }
    
    
    // пример создания нового кошелька
    
    func createNew() {
        Bitcore.createNew() {err, bitcore in
            if(err != nil) {
                self.createNewError(err: err!);
            }
            else {
                self.createNewSuccess(bitcore: bitcore!);
            }
        }
    }
    
    func createNewError(err: BitcoreError) {
        print("error:", err.code);
    }
    
    func createNewSuccess(bitcore: Bitcore) {
        self.bitcore = bitcore;
        //self.sendMoneyExample();
    }
    
    // пример импортирование из данных которые получены при выполнении bitcore.export
    func importFromData() {
        let aliceCredits = try! String(contentsOfFile: Bundle.main.path(forResource: "alice-credits", ofType: "json")!);
        
        bitcore = Bitcore(withJsonCredits: aliceCredits, isProduction: false);
    }
    
    // пример импортирования из "мнемоники"
    func importFromMnemonic() {
        let aliceWords = "brave model accident traffic auto tattoo faith wrestle wasp fuel dish arch";
        
        Bitcore.fromMnemonic(words: aliceWords){err, api in
            if(err != nil) {
                self.importFromMnemonicError(err: err!);
            }
            else {
                self.importFromMnemonicSuccess(bitcore: api!);
            }
        };
        
    }
    
    
    func importFromMnemonicSuccess(bitcore: Bitcore) {
        self.bitcore = bitcore;
        
        let credits = bitcore.export();
        //self.sendMoneyExample();
        print("success import mnemonic", credits);
    }
    
    
    func importFromMnemonicError(err: BitcoreError) {
        var errStr = "Неизвестная ошибка";
        
        if(err.code == BitcoreErrorCode.InvalidBackup) {
            errStr = "Невалидная мнемоника"
        }
        
        print("error", errStr);
    }
    
    func listenNotificationsExample() {
        self.bitcore?.setNotificationListener(interval: 1){ (notification) in
            // при получении нового уведомления, сюда прилетает
            
            if(notification.type == BitcoreNotificationType.NewIncomingTx) {
                let txid = notification.data["txid"] as! String;
                self.bitcore?.subscribeTxConfirmation(txId: txid) {err, res in
                    print("subcribed on \(txid)", err);
                }
            }
        }
    }
    
    
    // пример отправки монет
    func sendMoneyExample() {
        //bitcore.hello();
        bitcore?.sendMoney(
            toAddress: self.addressTxt.text!,
            amount: 100000,
            message: "Hello"
        ){(err, txp) -> () in
            if(err != nil) {
                self.sendMoneyError(err: err!);
            }
            else {
                self.sendMoneySuccess(txp: txp!);
            }
        };
    }
    
    
    func sendMoneySuccess(txp: BitcoreTransaction) {
        print("transaction created", txp);
        
        let alert = UIAlertController(title: "Success", message: "Transaction \(txp.txid)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*
        При отправке монет могут вернутся несколько ошибок
        InsufficientFunds - Недостаточно монет на счету
        UnknownError - Неивестная ошибка
     */
    func sendMoneyError(err: BitcoreError) {
        var errStr = "Неизвестная ошибка";
        
        if(err.code == BitcoreErrorCode.InsufficientFunds) {
            errStr = "Недостаточно средств"
        }
        
        print("error", errStr);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBtn(_ sender: Any) {
        print("Hello");
    
        sendMoneyExample();
    }


}

